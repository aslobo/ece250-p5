/*****************************************
 * Instructions
 *  - Replace 'uwuserid' with your uWaterloo User ID
 *  - Select the current calendar term and enter the year
 *  - List students with whom you had discussions and who helped you
 *
 * uWaterloo User ID:  aslobo@uwaterloo.ca
 * Submitted for ECE 250
 * Department of Electrical and Computer Engineering
 * University of Waterloo
 * Calender Term of Submission:  Winter 2015
 *
 * By submitting this file, I affirm that
 * I am the author of all modifications to
 * the provided code.
 *
 * The following is a list of uWaterloo User IDs of those students
 * I had discussions with in preparing this project:
 *    -
 *
 * The following is a list of uWaterloo User IDs of those students
 * who helped me with this project (describe their help; e.g., debugging):
 *    -
 *****************************************/
#ifndef WEIGHTED_GRAPH_H
#define WEIGHTED_GRAPH_H

#ifndef nullptr
#define nullptr 0
#endif

#include <iostream>
#include <limits>
#include <set>
#include "Exception.h"
#include "Disjoint_sets.h"

class Weighted_graph {

	/**
	 * Struct to store a weighted graph edge.
	 */
	struct edge_t {
		int v1;
		int v2;
		double weight;
		//Edges must be sorted by weight
		bool operator<(const edge_t& a) const {
			return weight <= a.weight;
		}
		//Two edges are equal if the join the same verticies
		bool operator() (const edge_t &e) const {
			return v1 == e.v1 && v2 == e.v2;
		}
		//Convenience constructor. Weight defaults to 0.0
		edge_t(int v1i, int v2i, double wi = 0.0) : v1(v1i), v2(v2i), weight(wi) {
		}
	};


	private:
		static const double INF;
		int verticies_count;
		std::set<edge_t> *edge_holder;

		// Do not implement these functions!
		// By making these private and not implementing them, any attempt
		// to make copies or assignments will result in errors
		Weighted_graph( Weighted_graph const & );
		Weighted_graph &operator=( Weighted_graph );

	public:
		Weighted_graph( int = 10 );
		~Weighted_graph();

		int degree( int ) const;
		int edge_count() const;
		std::pair<double, int> minimum_spanning_tree() const;

		bool insert_edge( int, int, double );
		bool erase_edge( int, int );
		void clear_edges();

	// Friends
	friend std::ostream &operator<<( std::ostream &, Weighted_graph const & );
};

const double Weighted_graph::INF = std::numeric_limits<double>::infinity();

Weighted_graph::Weighted_graph( int n ):
			verticies_count(n)
{
	edge_holder = new std::set<edge_t>();
}

Weighted_graph::~Weighted_graph() {
	delete edge_holder;
}


/*
 * int Weighted_graph::degree(int i)
 *
 * Returns an int representing the degree of the specified vertex
 *
 * Preconditions:
 *  i must be on the range [0, n-1]. illegal_argument is thrown if it is not
 *
 * Returns:
 *  int representing the degree of the specified vertex
 */
int Weighted_graph::degree( int i ) const {
	if (i < 0 || i > verticies_count-1) throw illegal_argument();

	int deg = 0;
	std::set<edge_t>::iterator it = edge_holder->begin();
	while(it != edge_holder->end()) {
		if (it->v1 == i || it->v2 == i) ++deg;
		it++;
	}

	return deg;
}

/*
 * int Weighted_graph::edge_count()
 *
 * Returns an int representing the number of edges in the graph
 *
 * Preconditions:
 *  None
 *
 * Returns:
 *  int representing the number of edges in the graph
 */
int Weighted_graph::edge_count() const {
	return edge_holder->size();
}

/*
 * std::pair<double, int> Weighted_graph::minimum_spanning_tree()
 *
 * Finds the minimium spanning tree using Kruskal's algorithm
 *
 * Preconditions:
 *  i and j must be on the range [0, n-1]. illegal_argument is thrown if they are not
 *
 * Returns:
 *  std::pair<double, int> representing the weight of the minimum spanning tree,
 *  and the number of edges tested to find the minimum spanning tree
 */
std::pair<double, int> Weighted_graph::minimum_spanning_tree() const {

	Data_structures::Disjoint_sets dset( verticies_count );

	int edges_tested = 0;
	double min_tree_weight = 0.0;

	std::set<edge_t>::iterator it = edge_holder->begin();
	while(it != edge_holder->end() && dset.disjoint_sets() != 1) {
		if (dset.find(it->v1) != dset.find(it->v2)) {
			dset.set_union(it->v1, it->v2);
			min_tree_weight += it->weight;
		}
		++edges_tested;
		++it;
	}

	return std::pair<double, int>( min_tree_weight, edges_tested );
}

/*
 * bool Weighted_graph::insert_edge(int i, int j, double d)
 *
 * Inserts an edge joining vertex i and j, with weight d
 *
 * Preconditions:
 *  i and j must be on the range [0, n-1]. illegal_argument is thrown if they are not
 *
 * Returns:
 *  bool representing if the edge was inserted successfully
 */
bool Weighted_graph::insert_edge( int i, int j, double d ) {
	//Ensure input verticies are valid
	if (i < 0 || i > verticies_count-1 || j < 0 || j > verticies_count-1 || d <= 0) throw illegal_argument();

	//Ensure that i <= j
	if (i > j) std::swap(i, j);

	//Check if i == j and this is already in the graph
	if (i == j) return false; //if (i == j && it != edge_holder->end()) return false;

	//Remove any duplicates in the set
	std::set<edge_t>::iterator it = edge_holder->find(edge_t(i,j));
	if(it != edge_holder->end()) {
		edge_holder->erase(it);
	}

	std::pair<std::set<edge_t>::iterator,bool> res = edge_holder->insert(edge_t(i, j, d));

	return res.second;
}

/*
 * bool Weighted_graph::erase_edge(int i, int j)
 *
 * Erases the edge joining vertex i and j
 *
 * Preconditions:
 *  i and j must be on the range [0, n-1]. illegal_argument is thrown if they are not
 *
 * Returns:
 *  bool representing if the edge was found and erased
 */
bool Weighted_graph::erase_edge( int i, int j ) {
	//Ensure input verticies are valid
	if (i < 0 || i > verticies_count-1 || j < 0 || j > verticies_count-1) throw illegal_argument();

	if (i == j) return true;

	if (i > j) std::swap(i, j); // Ensure that i < j

	//Find and erase edge if in the set
	std::set<edge_t>::iterator it = edge_holder->find(edge_t(i,j));
	if(it != edge_holder->end()) {
		edge_holder->erase(it);
		return true;
	}

	return false;
}

/*
 * void Weighted_graph::clear_edges()
 *
 * Clears all edges
 *
 * Preconditions:
 *  None
 *
 * Returns:
 *  Nothing
 */
void Weighted_graph::clear_edges() {
	edge_holder->clear();
}

std::ostream &operator<<( std::ostream &out, Weighted_graph const &graph ) {
	return out;
}

#endif
